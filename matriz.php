<?php

/**
 * MatrixDiagonals
 *
 */
class MatrixDiagonals
{
	protected $matrix = array();

	public function __construct()
	{
		$this->matrix[0][0] = 0;
		$this->matrix[0][1] = 1;
		$this->matrix[0][2] = 2;
		
		$this->matrix[1][0] = 3;
		$this->matrix[1][1] = 4;
		$this->matrix[1][2] = 5;
		
		$this->matrix[2][0] = 6;
		$this->matrix[2][1] = 7;
		$this->matrix[2][2] = 8;
	}

	public function getMatrix()
	{
		return $this->matrix;
	}

	/**
	* @TODO show diagonals
	*/
	public function showDiagonals()
	{
		for ($i = 0; $i <= 2; $i++) {

			for ($j = 0; $j <= $i; $j++) {

				echo $this->matrix[$j][$i] . ', ';

				if ($i == 0) {
					echo '<br />';
				}

				if ($j < $i) {
					echo $this->matrix[$i][$j] . ', <br />';			
				}

			}
		}

		echo '<br /><br />';
	}

	public function rotateMatrix()
	{
		array_unshift($this->matrix, null);
		$this->matrix = call_user_func_array('array_map', $this->matrix);
		$this->matrix = array_map('array_reverse', $this->matrix);
	}
}

$matrix = new MatrixDiagonals();

$matrix->showDiagonals();

$matrix->rotateMatrix();
$matrix->showDiagonals();

$matrix->rotateMatrix();
$matrix->showDiagonals();

$matrix->rotateMatrix();
$matrix->showDiagonals();