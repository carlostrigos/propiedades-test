<?php

namespace CTrigos\FoursquareBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PlacesController extends Controller
{
    protected $currentUserLocation = 'Toluca, MX';

    /**
     * @Route("/places/search", name="search_places")
     */
    public function showPlacesAction(Request $request)
    {
        $response       = null;
        $photos         = array();
        $userLocation   = ($request->get('location') != null) ? $request->get('location') : $this->currentUserLocation;
        $category       = ($request->get('category') != null) ? $request->get('category') : '';

        $results = $this->searchPlaces($userLocation, $category);

        if ($responseArray = $results) {
            $response = $responseArray;
            $photos = $this->getPhotosByPlace($responseArray['response']['venues']);
        }
        $parameters = array(
            'userLocation'          => $userLocation,
            'foursquareResponse'    => $response,
            'photos'                => $photos,
            'categories'            => $this->getFoursquareCategories(),
            'categoryName'          => $category
        );

        return $this->render('FoursquareBundle:Places:list.html.twig', $parameters);
    }

    /**
     * @return mixed
     */
    private function getFoursquareCategories()
    {
        $client = $this->container->get('jcroll_foursquare_client');
        $command = $client->getCommand('venues/categories');

        $results = $client->execute($command);

        return $results;

    }

    /**
     * @param $userLocation
     * @param string $category
     * @return mixed
     */
    private function searchPlaces($userLocation, $category = '')
    {
        $client = $this->container->get('jcroll_foursquare_client');
        $searchParams = array('near' => $userLocation);

        if ($category != '') {
            $searchParams['query'] = $category;
        }

        $command = $client->getCommand('venues/search', $searchParams);

        $results = $client->execute($command);

        return $results;
    }

    /**
     * @param array $places
     * @return array
     */
    private function getPhotosByPlace(Array $places)
    {
        $photos = array();
        $client = $this->container->get('jcroll_foursquare_client');

        foreach ($places as $place) {
            $placeId = $place['id'];
            $command = $client->getCommand('venues/photos', array(
                'venue_id' => $placeId
            ));

            if ($response = $client->execute($command)['response']) {
                $photos[$placeId] = $response;
            }
        }

        return $photos;
    }
}
