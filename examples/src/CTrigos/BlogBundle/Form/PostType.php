<?php

namespace CTrigos\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class PostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Titulo',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'required'  => true
            ))
            ->add('message', TextareaType::class, array(
                'label' => 'Mensaje',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'required'  => true
            ))
            ->add('photo', UrlType::class, array(
                'label' => 'Url de la Foto',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('video', UrlType::class, array(
                'label' => 'Url del video',
                'attr' => array(
                    'class' => 'form-control'
                )
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CTrigos\BlogBundle\Entity\Post'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ctrigos_blogbundle_post';
    }


}
