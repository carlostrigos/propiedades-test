<?php

namespace CTrigos\BlogBundle\Controller;

use CTrigos\BlogBundle\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BlogController extends Controller
{

    /**
     * @Route("/blog", name="blog")
     */
    public function blogAction()
    {
        $em     = $this->getDoctrine()->getManager();
        $posts  = $em->getRepository('BlogBundle:Post')->findAll();

        return $this->render('BlogBundle:Blog:index.html.twig', array(
            'posts' => $posts
        ));
    }

    /**
     * @Route("/blog/admin/post", name="blog_post")
     */
    public function blogPostAction()
    {
        return $this->render('BlogBundle:Blog:createPost.html.twig');
    }

    /**
     * Finds and displays a post entity.
     *
     * @Route("/blog/post/{id}", name="blog_post_show")
     */
    public function showAction(Post $post)
    {
        return $this->render('BlogBundle:Blog:show.html.twig', array(
            'post' => $post,
        ));
    }

}
